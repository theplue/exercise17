
/**
 * An item that has a weight an profit
 * @author Robert Slavik
 * @author Josh Coyle
 */
public class item {
    int number;
    int weight;
    int profit;
    double wp;
   /**
    * constructor to create an item
    * @param w - the weight of an item
    * @param p - the profit of an item
    * @param n - the number of an item
    */
    public item(int w, int p, int n){
     weight = w;
     profit = p;
     number = n;
     //the weight to profit ratio
     wp = (profit/weight);  
    }
}
