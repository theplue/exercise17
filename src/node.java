
import java.util.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Robert Slavik
 * @author Josh Coyle
 */
public class node {
    //number of the node
    final int number;
    //the level of the node
    final int level;
    //the weight of the current node
    final int weight;
    //the profit of the current node
    final int profit;
    //the potential(bound) of the node
    final double bound;
    //items being used
    final List<item> list;
    //items remaining on the list
    final List<item> remaining;
    /**
     * used to create a string representation of the items contained in each
     * node.
     * @return a string of the item numbers contained in the node 
     */
    @Override
    public String toString() {
        String s = "";
        if (list.isEmpty()) {
            return "-";
        }
        for (int i = 0; i < list.size(); i++) {
            s = s.concat(Integer.toString(list.get(i).number) + " ");
        }
        return s;
    }
    /**
     * the constructor of a new node
     * @param n - the number of the node
     * @param lev - the level of the node
     * @param wei - the weight of the node
     * @param pro - the profit of the node
     * @param b - the bound of the node
     * @param l - the list of items used by this node
     * @param r - the remaining items available for use
     */
    public node(int n, int lev, int wei, int pro, double b, List l, List r) {
        number = n;
        level = lev;
        weight = wei;
        profit = pro;
        bound = b;
        list = new ArrayList(l);
        remaining = new ArrayList(r);
    }
    /**
     * A node copier
     * @param n - the node to be copied
     */
    public node( node n){
        number = n.number;
        level = n.level;
        weight = n.weight;
        profit = n.profit;
        bound = n.bound;
        list = new ArrayList(n.list);
        remaining = new ArrayList(n.remaining);
    }
}
