
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is our attempt at exercise 17, the knap-sack problem
 * The program will print out each node explored, that nodes children, and 
 * upon completion, it will print out the best score, and the items to be 
 * used.
 * @author Robert Slavik
 * @author Josh Coyle 
 */
public class ex17 {

    //node counter
    private int counter = 0;
    //best score 
    private int best = 0;
    private node bnode;
    //master list of nodes
    private List<node> masterNode = new ArrayList();
    //master list of items
    private List<item> masterItem = new ArrayList();
    //the weight limit of the sack
    private static int limit;
    //the number of items in the sack
    static int n;
    /**
     * To read in an input file and create a list of items to be
     * used in the knap-sack problem
     * @param fileName - the file to be read in
     */
    public ex17(String fileName) {
        try {
            Scanner input = new Scanner(new File(fileName));
            limit = input.nextInt();//set limit from file
            n = input.nextInt();//set number of items to follow
            for (int i = 0; i < n; i++) {
                int tempp = input.nextInt();
                int tempw = input.nextInt();
                //create item, and add to master list
                masterItem.add(new item(tempw, tempp, i));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ex17.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    /**
     * method to be called to set the bound of the node to be created
     * as the children of the current node
     * @param list - of the items available to be used for creation of 
     * a nodes bound
     * @return the bound of the items on the list
     */
    public double setBound(List<item> list) {
        //temp weight
        int tempw = 0;
        //temp bound
        double tempb = 0;
        //counter
        int c = 0;
        //weightremaining
        int wr;
        List<item> temp = new ArrayList(list);

        for (int i = 0; i < temp.size(); i++) {
            if ((tempw + temp.get(i).weight) > limit) {
                wr = limit - tempw;
                tempb += (wr * temp.get(c).wp);
                return tempb;
            } else {
                tempw += temp.get(i).weight;
                tempb += temp.get(i).profit;
                c++;
            }
        }
        return tempb;
    }

    /**
     * To sort the list of  nodes with highest bound first
     * @param listNode - the list of nodes to be sorted 
     */
    public void sort(List<node> listNode) {
        int i, j, first;
        node temp;
        for (i = listNode.size() - 1; i > 0; i--) {
            first = 0;   //initialize to subscript of first element
            for (j = 1; j <= i; j++) //locate smallest element between positions 1 and i.
            {
                if (listNode.get(i).bound < listNode.get(first).bound) {
                    first = j;
                }
            }
            temp = listNode.get(first);   //swap smallest found with element in position i.
            listNode.set(first, listNode.get(i));
            listNode.set(i, temp);
        }
    }

    /**
     * used to print out the parent and 2 children node with their number,
     * weight, profit, level and bound
     * @param current - the parent node
     * @param lchild - the left child
     * @param rchild - the right child
     */
    public void print(node current, node lchild, node rchild) {

        System.out.println("Exploring node " + current.number + " items "
                + current.toString() + " level: " + current.level + " profit: "
                + current.profit + " weight: " + current.weight + " bound: "
                + current.bound);
        System.out.println("left child node " + lchild.number + " items "
                + lchild.toString() + " level: " + lchild.level + " profit: "
                + lchild.profit + " weight: " + lchild.weight + " bound: "
                + lchild.bound);
        System.out.println("right child node " + rchild.number + " items "
                + rchild.toString() + " level: " + rchild.level + " profit: "
                + rchild.profit + " weight: " + rchild.weight + " bound: "
                + rchild.bound);
        System.out.println();
    }

    /**
     * creates the children nodes of the current node, by using
     * or not using the next item on the list.
     * @param current 
     */
    public void children(node current) {
        //left child parts
        int pro = current.remaining.get(0).profit + current.profit;
        int weight = current.remaining.get(0).weight + current.weight;
        int lev = current.level + 1;
        //increment the node counter
        counter++;
        List<item> lchildlist = new ArrayList(current.list);
        lchildlist.add(current.remaining.get(0));
        List<item> lremaining = new ArrayList(current.remaining);
        lremaining.remove(0);
        double b = setBound(copy(lchildlist, lremaining));
        node lchild = new node(counter, lev, weight, pro, b, lchildlist, lremaining);
        //add left child to list
        masterNode.add(lchild);
        
        //right child parts
        List<item> rremaining = new ArrayList(lremaining);
        double rb = setBound(copy(current.list, rremaining));
        counter++;
        node rchild = new node(counter, lev, current.weight, current.profit, rb, current.list, rremaining);
        //add right child to list
        masterNode.add(rchild);
        //sort the list
        sort(masterNode);
        print(current, lchild, rchild);
    }
    /**
     * copy items from 2 list to one list
     * @param used - a list of item used
     * @param rem - a list of item still available for use
     * @return a combine list of all items in each node
     */
    public List copy(List<item> used, List<item> rem) {
        List<item> temp1 = new ArrayList(used);
        List<item> temp2 = new ArrayList(rem);
        temp1.addAll(temp2);

        return temp1;
    }
    /**
     * The main that runs creation of the list and prints out the steps
     * @param args 
     */
    public static void main(String[] args) {
        String fname = "";
        if (args.length != 1) {
            fname = FileBrowser.chooseFile(true);
        } else {
            fname = args[0];
        }
        ex17 exercise = new ex17(fname);
        //the initial node
        node root = new node(0, 0, 0, 0, exercise.setBound(exercise.masterItem), new ArrayList(), exercise.masterItem);
        exercise.masterNode.add(root);

        while (root.remaining.size() > 0) {
            if (root.weight <= limit) {
                exercise.children(root);
                if (root.profit >= exercise.best && root.weight != 0) {
                    exercise.best = root.profit;
                    exercise.bnode = new node(root);
                    exercise.masterNode.remove(root);
                    root = exercise.masterNode.get(0);
                } else if (exercise.masterNode.get(0).remaining.isEmpty()) {
                    System.out.println(" best score:" + exercise.bnode.profit + " with list of items: " + exercise.bnode.toString());
                    System.exit(0);
                } else {
                    exercise.masterNode.remove(root);
                    root = exercise.masterNode.get(0);
                }
            } else if (root.weight > limit) {
                System.out.println("overwieght node " + root.number);
                if (exercise.masterNode.get(1).remaining.isEmpty()) {
                    System.out.println("best score: " + exercise.best + " with the list "
                            + exercise.bnode.toString());
                    System.exit(1);
                } else {
                    exercise.masterNode.remove(0);
                    root = exercise.masterNode.get(0);
                }
            }
        }

        System.out.println("Best score: " + exercise.bnode.profit + " with list of items: " + exercise.bnode.toString());
        System.exit(0);
    }
}